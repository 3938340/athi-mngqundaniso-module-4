import 'package:flutter/material.dart';

import 'dashboard.dart';

void main(){
  runApp(const MyApp());
}
class MyApp extends StatelessWidget{
  const MyApp({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'DBSS',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Scaffold(
        appBar: AppBar( title: const Text("DBSS Register")),
        body: RegisterA(),
      )
    );
  }
}
class RegisterA extends StatefulWidget{
  @override
  _RegisterAState createState() => _RegisterAState();
}

class _RegisterAState extends State<RegisterA>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
        body: SafeArea(
            child:ListView(
                padding: const EdgeInsets.symmetric(horizontal: 14.0),
                children: <Widget>[
                  Column(
                    children: const <Widget>[
                      SizedBox(height: 80,),
                      SizedBox(height: 25,),
                      Text('Fill in the form below to register:',style: TextStyle(fontSize: 15, color: Colors.orangeAccent),)
                    ],
                  ),
                  const SizedBox(height:50.0,),
                  const TextField(
                    decoration: InputDecoration(
                      labelText: "Name",
                      labelStyle: TextStyle(fontSize:15),
                      filled: true,
                    ),
                  ),
                  const SizedBox(height:50.0,),
                  const TextField(
                    decoration: InputDecoration(
                      labelText: "Surname",
                      labelStyle: TextStyle(fontSize:15),
                      filled: true,
                    ),
                  ),
                  const SizedBox(height:50.0,),
                  const TextField(
                    decoration: InputDecoration(
                      labelText: "Username/Email",
                      labelStyle: TextStyle(fontSize:15),
                      filled: true,
                    ),
                  ),
                  const SizedBox(height:50.0,),
                  const TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(fontSize:15),
                      filled: true,
                    ),
                  ),
                  const SizedBox(height:15),
                  Column(
                      children: <Widget>[
                        MaterialButton(
                          height: 50,
                          disabledColor: Colors.grey,
                          disabledElevation: 4.0,
                          onPressed: () {Navigator.push(context, MaterialPageRoute(
                              builder: (context)=>DashboardA()
                          ));  },
                          child:const ElevatedButton(
                            onPressed: null,
                            child: Text('Register',style:TextStyle(fontSize:15,color: Colors.black)),
                          ),
                        ),
                      ]
                  )
                ]
            )
        )
    );
  }
}